<?php
function load_js() {

	wp_deregister_script('jquery');
	
	wp_enqueue_script(
		'angularjs',
		get_stylesheet_directory_uri() . '/node_modules/angular/angular.min.js'
	);
    
	wp_enqueue_script(
		'angularjs-route',
		get_stylesheet_directory_uri() . '/node_modules/angular-route/angular-route.min.js'
	);
    
    wp_enqueue_script(
		'moment',
		get_stylesheet_directory_uri() . '/node_modules/moment/min/moment-with-locales.min.js'
	);
    
    wp_enqueue_script(
		'load_angular',
		get_stylesheet_directory_uri() . '/app/assets/js/app.js',
		array( 'angularjs', 'angularjs-route' )
	);
    
    wp_enqueue_script( 
        'libs',
        get_template_directory_uri() . '/app/assets/js/libs.min.js',
        array(), '', true
    );
    
    wp_enqueue_script(
        'foundation-js',
        get_template_directory_uri() . '/app/assets/js/foundation.js',
        array( 'libs' ), '6.0', true
    );
    
    wp_enqueue_script(
        'site-js',
        get_template_directory_uri() . '/app/assets/js/scripts.js',
        array( 'libs' ), '', true
    );
    
    // Load App Frontpage Controller
    wp_enqueue_script(
		'main-ctrl',
		get_stylesheet_directory_uri() . '/app/assets/js/controllers/mainController.js'
	);
    
    // Load App Blog Controller
    wp_enqueue_script(
		'blog-ctrl',
		get_stylesheet_directory_uri() . '/app/assets/js/controllers/blogController.js'
	);
    
    // Load App Frontpage Controller
    wp_enqueue_script(
		'section-ctrl',
		get_stylesheet_directory_uri() . '/app/assets/js/controllers/sectionController.js'
	);
    
    // Load App Directives
    wp_enqueue_script(
		'app-directives',
		get_stylesheet_directory_uri() . '/app/assets/js/directives/directives.js'
	);
    
    // Load App Filters
    wp_enqueue_script(
		'app-filters',
		get_stylesheet_directory_uri() . '/app/assets/js/filters/filters.js'
	);
    
    wp_enqueue_script(
		'app-api',
		get_stylesheet_directory_uri() . '/app/assets/js/services/api.js'
	);

    wp_enqueue_style(
        'site-css',
        get_template_directory_uri() . '/app/assets/css/style.min.css',
        array(), '', 'all'
    );
    
    wp_localize_script(
		'load_angular',
		'myLocalized',
		array(
			'partials' => trailingslashit( get_template_directory_uri() ) . 'app/partials/',
            'nonce' => wp_create_nonce( 'wp_rest' )
			)
	);
}
add_action( 'wp_enqueue_scripts', 'load_js' );

    
function my_add_link_target( $html ) {
    $html = preg_replace( '/(<a.*")>/', '$1 target="_self">', $html );
    return $html;
}
add_filter( 'image_send_to_editor', 'my_add_link_target', 10 );


function my_rest_post_query( $args, $request ) {
	if ( isset( $request['filter'] ) && isset( $request['filter']['posts_per_page'] ) && ! empty( $request['filter']['posts_per_page'] ) ) {
		if ( $request['filter']['posts_per_page'] > 0 ) {
			$request['per_page'] = $request['filter']['posts_per_page'];
		} else {
			$count_query = new WP_Query();
			unset( $query_args['paged'] );
			$query_result = $count_query->query( $query_args );
			$total_posts = $query_result->found_posts;
			$request['per_page'] = $total_posts;
		}
	}
	return $args;
}
add_filter( 'rest_post_query', 'my_rest_post_query', 10, 2 );


function my_rest_prepare_post( $data, $post, $request ) {
	$_data = $data->data;
	$thumbnail_id = get_post_thumbnail_id( $post->ID );
	$thumbnail = wp_get_attachment_image_src( $thumbnail_id );
	$_data['featured_image_thumbnail_url'] = $thumbnail[0];
	$data->data = $_data;
	return $data;
}
add_filter( 'rest_prepare_post', 'my_rest_prepare_post', 10, 3 );

function sa_pag_links( $data, $post, $request ) {
    
    $_data = $data->data;
    
    // Load $post
    global $post;
    $post = get_post( $_post['ID']);
    
    // Initialize the previous and next posts
    $previous_post = get_adjacent_post( false, '', true );
    $next_post = get_adjacent_post( false, '', false );

    // Get the thumbnail previous link
    $prev_thumbnail_id = get_post_thumbnail_id( $previous_post->ID );
    $prev_thumbnail = wp_get_attachment_image_src( $prev_thumbnail_id );
    
    // Get the thumbnail next link
    $next_thumbnail_id = get_post_thumbnail_id( $next_post->ID );
    $next_thumbnail = wp_get_attachment_image_src( $next_thumbnail_id );
    
    // Add JSON Arrays for previous_post & next_post
    $previous_arr = array(
        'link' => get_permalink( $previous_post ),
        'title' => get_the_title( $previous_post ),
        'thumbnail' => $prev_thumbnail[0]
    );    
    $_data['previous_post'] = $previous_arr;
    
    $next_arr = array(        
        'link' => get_permalink( $next_post ),
        'title' => get_the_title( $next_post ),
        'thumbnail' => $next_thumbnail[0]
    );
	$_data['next_post'] = $next_arr;
    
    // Send to JSON
    $data->data = $_data;
    
    return $data;
}
add_filter( 'rest_prepare_post', 'sa_pag_links', 10, 4);

function sa_count_comments( $data, $post, $request ) {
    
    $_data = $data->data;
    
    // Load $post
    global $post;
    $post = get_post( $_post['ID']);
    
    // Comments counting
    $count = wp_count_comments( $post->ID ); 
    
    // Add JSON object for $count
    $_data['count_comments'] = $count;
    
    // Send to JSON
    $data->data = $_data;
    
    return $data;
}
add_filter( 'rest_prepare_post', 'sa_count_comments', 10, 5);

?>