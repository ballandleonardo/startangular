// *** Directives

app.directive('searchForm', function() {
    return {
        restrict: 'EA',
        templateUrl: myLocalized.partials + 'main_searchform.html',
        controller: function($scope, $http){
            $scope.filter = {
                s: ''
            };
            $scope.search = function(){
                if ($scope.filter.s.length >= 5) {
                        $http.get('wp-json/wp/v2/posts/?filter[s]=' + $scope.filter.s + '&filter[posts_per_page]=5').success(function(res){
                            $scope.posts = res;
                        });
                }
            };
        }
    };
});


app.directive('postFilter', function() {
    return {
        restrict: 'EA',
        templateUrl: myLocalized.partials + 'blog_postfilter.html',
        controller: function($scope, $http, $routeParams){                        
            $http.get('wp-json/wp/v2/terms/category').success(function(res){
                $scope.sections = res;
            });
        }
    };
});


app.directive('currentUser', function(){
	return {
		restrict: 'EA',
		templateUrl: myLocalized.partials + 'current-user.html',
		controller: ['apiService', '$scope', function(apiService, $scope) {
			apiService.getCurrentUser();
            $scope.data = apiService;
		}]
	};
});