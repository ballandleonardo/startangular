// *** Blog Controllers ***

// Display Posts List
app.controller('BlogController',['$scope', '$http', '$routeParams', 'apiService', function($scope, $http, $routeParams, apiService) {
        apiService.getAllPosts();
        $scope.data = apiService;
}]);

//************//    

// Display Single Post
app.controller('PostController',['$scope', '$http', '$routeParams', 'apiService', function($scope, $http, $routeParams, apiService) {
    $http.get('wp-json/wp/v2/posts/?filter[name]=' + $routeParams.slug).success(function(res){
        $scope.post = res[0];
        $http.get('wp-json/wp/v2/media/' + res[0].featured_image).success(function(res){
            $scope.bkg = res.guid.rendered; 
        });

        document.querySelector('title').innerHTML = $scope.post.title.rendered + ' - StartAngular';
        
        // Get comments list
        apiService.getComments(res[0].id);
        
        // Define the form object
        $scope.cf = {
            name: '',
            email: '',
            website: '',
            comment: ''
        };
        
        // Submit the new comment form
        $scope.send = function(){
            
            // Get the values of the comment form
            var form = {
                author_name: $scope.cf.name,
                author_email: $scope.cf.email,
                author_url: $scope.cf.website,
                content: $scope.cf.comment,
                post: res[0].id
            };
            
            // Send to post function of the API
            apiService.postComment(form);
            
            // Reset function
            $scope.reset = function(coform){
                if(coform){
                    coform.$setPristine();
                    coform.$setUntouched();
                }
                $scope.cf = {};
            };
            
            // Reset form
            $scope.reset();
        };
        
        $scope.data = apiService;
    });
}]);

//************//   

