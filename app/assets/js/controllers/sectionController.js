// *** Blog Controllers ***

app.controller('SectionController', ['$scope', '$http', '$routeParams', 'apiService', function($scope, $http, $routeParams, apiService) {
        apiService.getAllCategories();
        $http.get('wp-json/wp/v2/terms/category/?search=' + $routeParams.slug).success(function(res){
            $scope.current_section_id = res[0].id;
            apiService.getPostsInSection(res[0]);            
        });
    
        $scope.data = apiService;
}]);