// *** Main Controller ***

app.controller('MainController', ['$scope', function($scope) {
	$scope.mainTitle = "Frontpage";
    document.querySelector('title').innerHTML = 'StartAngular - An awesome starterkit';
}]);

// ******************************* //

app.controller('404', function(){
   document.querySelector('title').innerHTML = 'Page inconnue | StartAngular'; 
});