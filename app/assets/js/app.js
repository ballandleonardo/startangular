var app = angular.module('app', ['ngRoute']);

// App Configuration
app.config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
	$locationProvider.html5Mode(true);

	$routeProvider
	.when('/', {
		templateUrl: myLocalized.partials + 'main.html',
		controller: 'MainController'
	})
    .when('/blog/', {
        templateUrl: myLocalized.partials + 'blog.html',
        controller: 'BlogController'
    })
    .when('/blog/:slug', {
        templateUrl: myLocalized.partials + 'blog-post.html',
        controller: 'PostController'
    })
    .when('/section/:slug', {
        templateUrl: myLocalized.partials + 'blog.html',
        controller: 'SectionController'
    })
    .otherwise({
        templateUrl: myLocalized.partials + '404.html',
        controller: '404'
    });
    
    $httpProvider.interceptors.push([function(){
        return {
            'request': function(config){
                config.headers = config.headers || {};
                //add nonce to avoid CSRF issues
                config.headers['X-WP-Nonce'] = myLocalized.nonce;
                
                return config;
            }
        };
    }]);
}]);