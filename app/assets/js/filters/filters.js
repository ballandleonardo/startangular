// *** Filters

app.filter('toTrusted', ['$sce', function($sce) {
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.filter('fulldate', function() {
    return function(dateString) {
        return moment(dateString).locale('fr').format('LL');
    };
});