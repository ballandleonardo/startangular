app.factory('apiService', function($http) {

	var apiService = {
		pageTitle: '',
        posts: [],
        post: {},
        sections: [],
        comments: [],
        currentUser: {}
	};

	apiService.getAllPosts = function(){	
		return $http.get('wp-json/wp/v2/posts').success(function(res){
			apiService.posts = res;
            document.querySelector('title').innerHTML = "Blog - StartAngular";
		}).error(function(err, status){
            if(status === 404){
                $scope.is404 = true;
                document.querySelector('title').innerHTML = 'Page inconnue - StartAngular';
                $scope.errorMessage = 'Error : ' + err[0].message;
            }
        });
	};
    
    apiService.getPost = function(){
        $http.get('wp-json/wp/v2/posts/?filter[name]=' + post.name).success(function(res){
            apiService.post = res;
        })
    };
    
    apiService.getAllCategories = function(section){
        if(apiService.sections.length){
            return;
        }
        
        return $http.get('wp-json/wp/v2/terms/category/').success(function(res){
            apiService.sections = res;
        });
    };
    
    apiService.getPostsInSection = function(section){
        var url = 'wp-json/wp/v2/posts/?filter[category_name]=' + section.name + '&filter[posts_per_page]=-1';

		return $http.get(url).success(function(res){
            apiService.posts = res;
            document.querySelector('title').innerHTML = "Articles de la section " + section.name + " - StartAngular";
            apiService.pageTitle = "Articles dans la section : " + section.name;
		});
    };
    
    apiService.getComments = function(postID){
        $http.get('wp-json/wp/v2/comments/?post=' + postID).success(function(res){
            apiService.comments = res;
        });  
    };
    
    apiService.postComment = function(form){
        $http.post('wp-json/wp/v2/comments', form).success(function(res){
            apiService.comments.unshift(res);
        }).error(function(res){
            console.log("Erreur : " + res);                                       
        });
    };
    
    apiService.getCurrentUser = function(){
      $http.get('wp-json/wp/v2/users/me').success(function(res){
         apiService.currentUser = res;
      });
    };

	return apiService;
});