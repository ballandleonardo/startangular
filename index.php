<!DOCTYPE html>
<html>
<head>
    <base href="/sites/1tps/">
    <title>StartAngular</title>
    <?php wp_head(); ?>
</head>
<body>
   <div id="wrapper" ng-app="app">
       
        <header>
            <div class="top-bar">
                <div class="top-bar-left">
                    <ul class="menu">
                        <li><a href="<?php echo site_url(); ?>" class="menu-text">StartAngular</a></li>
                    </ul>
                </div>
                <div class="top-bar-right">
                    <ul class="menu">
                        <li><a href="<?php echo site_url(); ?>/blog">Blog</a></li>
                    </ul>
                </div>
            </div>        
        </header>
    
        <div ng-view></div>    

        <footer>
            &copy; <?php echo date( 'Y' ); ?>
        </footer>
    </div><!-- Wrapper -->
    
    <?php wp_footer(); ?>    
</body>
</html>