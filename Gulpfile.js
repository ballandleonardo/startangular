//* -----------------
//  GULPFILE.JS
//* -----------------

var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber');
    
//* -----------------
//  SASS TASKS
//* -----------------
gulp.task('sass', function() {
  return gulp.src('./app/assets/scss/**/*.scss')
    .pipe(plumber(function(error) {
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
    }))
    .pipe(sass())
    .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'],
            cascade: false
        }))
    .pipe(rename({basename: 'style'}))
    .pipe(gulp.dest('./app/assets/css/'))     
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('./app/assets/css/'))
});    
    
//* -----------------
//  JAVASCRIPT TASKS
//* -----------------
gulp.task('scripts', function() {
  return gulp.src([
      './app/assets/js/scripts/*.js'  		  
  ])
    .pipe(plumber())
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('./app/assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./app/assets/js'))
});    

gulp.task('libs', function() {
  return gulp.src([	
          './node_modules/jquery/dist/jquery.js',
          './node_modules/what-input/what-input.js'          
  ])
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('./app/assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./app/assets/js'))
});

//* -----------------
//  FOUNDATION TASKS
//* -----------------

gulp.task('foundation', function() {
  return gulp.src([	
  		  
          './node_modules/foundation-sites/js/foundation.core.js',
          './node_modules/foundation-sites/js/foundation.util.*.js',
          './node_modules/foundation-sites/js/foundation.abide.js',
          './node_modules/foundation-sites/js/foundation.accordion.js',
          './node_modules/foundation-sites/js/foundation.accordionMenu.js',
          './node_modules/foundation-sites/js/foundation.drilldown.js',
          './node_modules/foundation-sites/js/foundation.dropdown.js',
          './node_modules/foundation-sites/js/foundation.dropdownMenu.js',
          './node_modules/foundation-sites/js/foundation.equalizer.js',
          './node_modules/foundation-sites/js/foundation.interchange.js',
          './node_modules/foundation-sites/js/foundation.magellan.js',
          './node_modules/foundation-sites/js/foundation.offcanvas.js',
          './node_modules/foundation-sites/js/foundation.orbit.js',
          './node_modules/foundation-sites/js/foundation.responsiveMenu.js',
          './node_modules/foundation-sites/js/foundation.responsiveToggle.js',
          './node_modules/foundation-sites/js/foundation.reveal.js',
          './node_modules/foundation-sites/js/foundation.slider.js',
          './node_modules/foundation-sites/js/foundation.sticky.js',
          './node_modules/foundation-sites/js/foundation.tabs.js',
          './node_modules/foundation-sites/js/foundation.toggler.js',
          './node_modules/foundation-sites/js/foundation.tooltip.js',
          './node_modules/foundation-sites/js/motion-ui.js'
  ])
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(concat('foundation.js'))
    .pipe(gulp.dest('./app/assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./app/assets/js'))
});  

//* -----------------
//  DEFAULT TASK
//* -----------------

gulp.task('default', function() {
  gulp.start('sass', 'scripts', 'libs', 'foundation');
});

//* -----------------
//  WATCH TASK
//* -----------------

gulp.task('watch', function() {

  gulp.watch('./app/assets/scss/**/*.scss', ['sass']);
  gulp.watch('./app/assets/js/scripts/*.js', ['scripts']);
  gulp.watch('./node_modules/foundation-sites/js/*.js', ['foundation']);

});